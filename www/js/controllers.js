angular.module('starter.controllers', []).controller('HomeCtrl', function ($scope, Kards) {
    $scope.kards = Kards.all();
    $scope.remove = function (kard) {
        Kards.remove(kard);
    };
}).controller('SearchCtrl', function ($scope) {
    $scope.images = [];
    $scope.loadImages = function () {
        for (var i = 0; i < 20; i++) {
            $scope.images.push({
                id: i
                , src: "http://icons.iconarchive.com/icons/hopstarter/scrap/256/User-icon.png"
            });
        }
    }
}).controller('ProfileCtrl', function ($scope) {}).controller('ActivityCtrl', function ($scope) {}).controller('UploadCtrl', function ($scope, $ionicPlatform, $cordovaCamera) {
    $scope.getImageCamera = function () {
        var options = {
            quality: 75
            , destinationType: Camera.DestinationType.DATA_URL
            , sourceType: 1
            , allowEdit: true
            , encodingType: Camera.EncodingType.JPEG
            , targetWidth: 300
            , targetHeight: 300
            , popoverOptions: CameraPopoverOptions
            , saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function (imageData) {
            $scope.imgURI = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            // An error occured. Show a message to the user
        });
    };
    $scope.getImageGallery = function () {
        var options = {
            quality: 75
            , destinationType: Camera.DestinationType.DATA_URL
            , sourceType: 0
            , allowEdit: true
            , encodingType: Camera.EncodingType.JPEG
            , targetWidth: 300
            , targetHeight: 300
            , popoverOptions: CameraPopoverOptions
            , saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function (imageData) {
            $scope.imgURI = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            // An error occured. Show a message to the user
        });
    };
});