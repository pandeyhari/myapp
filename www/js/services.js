angular.module('starter.services', []).factory('Kards', function () {
    var kards = [{
        id: 0
        , name: 'Hari'
        , caption: '#ODL120vJuhla'
        , face: './img/image4.jpg'
        , img: './img/image7.jpg'
 }, {
        id: 1
        , name: 'Boss'
        , caption: '#Norway'
        , face: './img/image7.jpg'
        , img: './img/image4.jpg'
 }, {
        id: 2
        , name: 'Kripesh'
        , caption: 'Kuusamo'
        , face: './img/image8.jpg'
        , img: './img/image5.jpg'
 }, {
        id: 3
        , name: 'Hari'
        , caption: '#Skydive2015'
        , face: './img/image7.jpg'
        , img: './img/image6.jpg'
 }, {
        id: 4
        , name: 'Mikko'
        , caption: '#Nordkapp'
        , face: './img/image4.jpg'
        , img: './img/image8.jpg'
 }, {
        id: 5
        , name: 'Subash'
        , caption: 'Troms'
        , face: './img/image8.jpg'
        , img: './img/image4.jpg'
 }];
    return {
        all: function () {
            return kards;
        }
        , remove: function (kard) {
            kards.splice(kards.indexOf(kard), 1);
        }
        , get: function (kardId) {
            for (var i = 0; i < kards.length; i++) {
                if (kards[i].id === parseInt(kardId)) {
                    return kards[i];
                }
            }
            return null;
        }
    }
});